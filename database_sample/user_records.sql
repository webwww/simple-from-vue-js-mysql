-- phpMyAdmin SQL Dump
-- version 4.9.7deb1
-- https://www.phpmyadmin.net/
--
-- Хост: localhost:3306
-- Время создания: Апр 04 2021 г., 15:19
-- Версия сервера: 8.0.23-0ubuntu0.20.10.1
-- Версия PHP: 7.4.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `records`
--

-- --------------------------------------------------------

--
-- Структура таблицы `user_records`
--

CREATE TABLE `user_records` (
  `id` int NOT NULL,
  `name` varchar(255) NOT NULL,
  `comment` varchar(2000) NOT NULL,
  `timestamp` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Дамп данных таблицы `user_records`
--

INSERT INTO `user_records` (`id`, `name`, `comment`, `timestamp`) VALUES
(6, '', 'dawdfwe', 1617534537),
(7, '13123', 'dawdawd', 1617537947),
(8, '213', 'dasdad', 1617538003),
(9, 'dawd', 'dawdawda', 1617538660);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `user_records`
--
ALTER TABLE `user_records`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `user_records`
--
ALTER TABLE `user_records`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
