const express = require('express');
const app = express();
const axios = require('axios');
const mysql = require('mysql2');
const fs = require('fs');

const port = process.env.VUE_APP_APIPORT || 3111;

app.use(express.static('public'));
app.use(express.json());

app.use((req, res, next) => {
    res.setHeader("Access-Control-Allow-Origin", `http://localhost:${process.env.PORT}`);
    res.header(
      "Access-Control-Allow-Headers",
      "Origin, X-Requested-With, Content-Type, Accept"
    );
    next();
  });

app.post('/send-form', async function(req, res){

    res.header("Access-Control-Allow-Origin", "*");

    console.log(req.body)
    const name = req.body.name;
    const comment = req.body.comment;

    if (!name || !comment) {
        res.status(400).send(false);
    };

    if (name.length > 255 || comment.length > 2000) { // длина полей в базе
      res.status(400).send("too long string");
    }

    const dbConn = await createConnection();
    const dbResponce = await insertRecord(dbConn, name, comment);

    return res.status(200).json(`Результат записан под номером ${dbResponce.insertId}`);
});

app.listen(port, () => {
    console.log('server started on', port, 'port');
})

function createConnection(){

  try {
    this.options = JSON.parse(fs.readFileSync('api/config.json'));
  } catch (e) {
      console.log(e, 'Проверьте config.json');
  }

  return new Promise (function (resolve, reject) {
    const connection = mysql.createPool({
        host: this.options.host,
        user: this.options.user,
        database: this.options.database,
        password: this.options.password
    })
    resolve(connection)
  })
}

function insertRecord(connection, name, comment) {
  return new Promise(function (resolve, reject) {
      connection.promise().query(`
      insert into user_records values (default, ?, ?, UNIX_TIMESTAMP())
      `, [name, comment]).then( ([rows,fields]) => {resolve(rows)});
  })
}