# simple-form

### Поднять фронт и бэк с указанием портов (нужен pm2)
```
npm run start --port=4444 --apiport=5555 (можно указать любые порты)
```

1. npm install
2. npm install pm2 -g
3. Импортировать образец базы из database_sample (в уже запущенную mysql)
4. Указать данные базы в api/api.js
5. Запустить всю штуку: npm run start --port=4444 (на этом порте запустится фронт) --apiport=5555 (на этом порте запустится бэк)

### Есть версия с докером: https://gitlab.com/webwww/docker-mysql-vue-node
